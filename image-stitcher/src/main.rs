extern crate image;

use image::{GenericImageView, DynamicImage, GenericImage, Pixels};

struct BlockOffset {
    x: u32,
    y: u32,
}

struct ImagePosition {
    image: DynamicImage,
    offset: BlockOffset,
}

impl ImagePosition {
    fn new(image: DynamicImage, x: u32, y: u32) -> ImagePosition {
        let (xSize, ySize) = image.dimensions();

        ImagePosition {
            image: image,
            offset: BlockOffset {
                x: x * xSize,
                y: y * ySize,
            },
        }

    }
}

fn main() {
    // Use the open function to load an image from a Path.
    // ```open``` returns a `DynamicImage` on success.
   // let img1 = image::open("/Users/vs45bp/Downloads/mecaniques/0_0.jpg").unwrap();
   // let img2 = image::open("/Users/vs45bp/Downloads/mecaniques/0_1.jpg").unwrap();

    let blocks: Vec<ImagePosition> = {
        let mut vector: Vec<ImagePosition> = Vec::with_capacity(110);
        for x in 0..10 {
            for y in 0..9 {
                let img = image::open(format!("/Users/vs45bp/Downloads/mecaniques/{}_{}.jpg", x, y)).unwrap();
                let image_position = ImagePosition::new(img, x, y);
                println!("Position: {} {}", image_position.offset.x, image_position.offset.y);
                vector.push(image_position);
            }
        }
        vector
    };

    //let dimensions = image::image_dimensions("/Users/vs45bp/Downloads/mecaniques/0_0.jpg").unwrap();

    let mut result = DynamicImage::new_rgb8(blocks.last().unwrap().offset.x + blocks[11].offset.x, blocks.last().unwrap().offset.y + blocks[1].offset.y);

    println!("Image size: {} by {}", result.dimensions().0, result.dimensions().1);

    for image in blocks {
        for pixel in image.image.pixels() {
            result.put_pixel(image.offset.x + pixel.0, image.offset.y + pixel.1, pixel.2)
        }
    }

   // println!("Vector result: {:?}", result);

    // The dimensions method returns the images width and height.
//    println!("dimensions {:?}", img1.dimensions());

    // The color method returns the image's `ColorType`.
 //   println!("{:?}", img1.color());


//    let mut result = DynamicImage::new_rgb8(img1.width(), img1.height() + img2.height());
//    for pixel in img1.pixels() {
//        result.put_pixel(pixel.0, pixel.1, pixel.2)
//    }

    println!("Saving result... ");
    // Write the contents of this image to the Writer in PNG format.
    result.save("test.png").unwrap();
}
