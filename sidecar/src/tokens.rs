use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::{http, Error, HttpResponse};
use futures::future::{ok, Either, FutureResult};
use futures::Poll;
use http::header::HeaderValue;

use actix_router::Router;

//#[macro_use]
//extern crate serde_derive;

pub struct CheckPeerToken;

impl<S, B> Transform<S> for CheckPeerToken
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = CheckPeerTokenMiddleware<S>;
    type Future = FutureResult<Self::Transform, Self::InitError>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(CheckPeerTokenMiddleware { service })
    }
}
pub struct CheckPeerTokenMiddleware<S> {
    service: S,
}

impl<S, B> Service for CheckPeerTokenMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Either<S::Future, FutureResult<Self::Response, Self::Error>>;

    fn poll_ready(&mut self) -> Poll<(), Self::Error> {
        self.service.poll_ready()
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        // We only need to hook into the `start` for this middleware.
        match req.headers().get("JWT") {
            None => Either::B(ok(
                req.into_response(HttpResponse::Unauthorized().finish().into_body())
            )),
            Some(token) if is_valid(token, &req) => Either::A(self.service.call(req)),
            _ => Either::B(ok(
                req.into_response(HttpResponse::BadRequest().finish().into_body())
            )),
        }
    }
}

extern crate jsonwebtoken as jwt;
use jwt::{encode, decode, Header, Algorithm, Validation};
use jwt::TokenData;

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
//#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    sub: String,
    company: String,
    exp: usize,
}

// Match is already made, since middleware is touched. only validate jwt
fn is_valid(token_value: &HeaderValue, req: &ServiceRequest) -> bool {
    println!("Requst path: {}", req.path());
    println!("Requst match: {:?}", req.match_info());

    let token_string = token_value.to_str().unwrap();

  //  let claims = decode::<Claims>(&token_string, "secret".as_ref(), &Validation::default()).unwrap();


    true
}
