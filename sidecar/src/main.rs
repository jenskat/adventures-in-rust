use actix_web::{web, http, App, HttpResponse, Error, HttpServer, Responder, Resource, guard};
use actix_web::middleware::Logger;
use env_logger;
use futures::Future;

#[macro_use]
extern crate log;

mod tokens;

use openssl::ssl::{SslAcceptor, SslAcceptorBuilder, SslFiletype, SslMethod, SslVerifyMode};
use std::prelude::v1::ToString;
//use openssl::x509::extension::{
  //  AuthorityKeyIdentifier, BasicConstraints, ExtendedKeyUsage, KeyUsage, SubjectAlternativeName,
  //  SubjectKeyIdentifier,
//};
//use openssl::x509::{X509Name, X509Req, X509StoreContext, X509VerifyResult, X509};


fn main() {
        std::env::set_var("RUST_LOG", "actix_web=debug");

    let mut builder: SslAcceptorBuilder = SslAcceptor::mozilla_modern(SslMethod::tls()).unwrap(); //_v5 is not there yet.
    let mut verify_mode = SslVerifyMode::empty();
verify_mode.insert(SslVerifyMode::PEER);
verify_mode.insert(SslVerifyMode::FAIL_IF_NO_PEER_CERT);

    builder
        .set_private_key_file("server_key.pem", SslFiletype::PEM)
        //  set_ca_file
        .unwrap();

    builder.set_certificate_chain_file("server_cert.pem").unwrap();
// TODO Check how to setup mutual authentication (cacerts needed?)
    builder.set_ca_file("server_cert.pem").unwrap();
    builder.set_verify(SslVerifyMode::PEER | SslVerifyMode::FAIL_IF_NO_PEER_CERT);

    builder.check_private_key().unwrap();

    // let mut x509builder = X509::builder().unwrap();

    // let ext_key_usage = ExtendedKeyUsage::new()
    //     .client_auth()
    //     .server_auth()
    //     .build()
    //     .unwrap();

    // x509builder.append_extension(ext_key_usage).unwrap();

    env_logger::init();
    info!("starting up");

    let services: Vec<Service> = vec!(
        Service{ path_template: "/call/{id}/me".to_string(), method : "GET".to_string()},
        Service{ path_template: "/call/{id}/me".to_string(), method : "POST".to_string()},
        Service{ path_template:"/catch".to_string(), method : "DELETE".to_string()},
        );
        // TODO transform service to groupedservice
    let grouped_services: Vec<GroupedService> = vec!(
        GroupedService{ path_template: "/call/{id}/me".to_string(), methods : vec!("GET".to_string(), "POST".to_string())},
        GroupedService{ path_template:"/catch".to_string(), methods : vec!("DELETE".to_string())},
        );

    // let resources: Vec<Resource> = resource_builder(services);

    // let resource: Resource = resources[0];
    //HttpServer::new(resources[0].default);

    //  let t = Router::build();
    // t.path(path: &str, resource: T);

    //let my_app = App::new();
    //my_app.configure(f: F)

    //let http_server = HttpServer::new(|| {my_app});

    HttpServer::new(move || { // Move moves the owning of the data to the closure. every thread starts its own app?
        let mut app =         App::new()
                // setup builtin logger to get nice logging for each request
            .wrap(Logger::default())
            // TODO tracing middleware (wrap_fn?)
            // peertoken middleware
            .wrap(tokens::CheckPeerToken);
        //    .service(r)
        // TODO check order of middleware, reverse or not.

        let resources = resource_builder(&grouped_services);
        let iter = resources.into_iter();

        for r in iter {
            app = app.service(r)
        }

        app

    })
     .bind_ssl("localhost:8088", builder)
    // .bind("localhost:8089")
    .unwrap()
    .run()
    .unwrap();
}

#[derive(Clone, Debug)]
struct Service {
    path_template: String,
    method: String,
}

#[derive(Clone, Debug)]
struct GroupedService {
    path_template: String,
    methods: Vec<String>,
}
impl PartialEq for Service {
    fn eq(&self, other: &Self) -> bool {
        self.path_template == other.path_template && self.method == other.method
    }
}
impl Eq for Service {}

// use std::collections::HashMap;
//     fn from_services(services: Vec<Service>) -> HashMap<String, Vec<String>> {
//         let mut result: HashMap<String, Vec<String>> = HashMap::new();
//         for service in services {
//             match result.get(&service.path_template) {
//                 None => result.insert(service.path_template, vec!(service.method)),
//                 Some(methods) => {
//                     let mut new_methods = &methods;
//                     new_methods.push(service.method);
//                     result.insert(service.path_template, methods.to_vec())
//                 }

//             };
//         }
//         //     if result.contains_key(&service.path_template) {
//         //         let mut values = result.get(&service.path_template).unwrap();
//         //         values.push(service.method);
//         //         result.insert(service.path_template, values);
//         //     } else {
//         //         result.insert(service.path_template, vec!(service.method));
//         //     }
//         // }
//         result
//     }



fn resource_builder(services: &[GroupedService]) -> Vec<Resource> {
    let mut resources = Vec::new();

    for s in services {
        let mut route = web::resource(&s.path_template);
        println!("s {:?}", s);

        for method in &s.methods {
            let m: http::Method = http::Method::from_bytes(method.as_bytes()).unwrap();
            let g = guard::Method(m);

            route = route.route(
                web::route()
                    .guard(g)
                    .to_async(async_index)
                );
        }

        resources.push(route);
    }
    resources
}



fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

fn async_index() -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        // do stuff...
               Ok::<_, std::io::Error>("response")
})
     .from_err()
        .and_then(|b| HttpResponse::Ok().body(format!("hello from async {}", b)))
}

fn index2() -> impl Responder {
    HttpResponse::Ok().body("Hello world again!")
}
